package com.josematias.sisgeer;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewDebug;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

public class ServerRequests {

    ProgressDialog progressDialog;
    public static final int CONNECTION_TIMEOUT = 1000 * 15;
    private static final String URL_LOGIN = "http://104.131.165.12/sisgeer/login.php";
    private static final String URL_REGISTER = "http://104.131.165.12/sisgeer/register.php";
    private static final String URL_UPDATE = "http://104.131.165.12/sisgeer/update.php";
    private static final String URL_CONFIGURATION = "http://104.131.165.12/sisgeer/configuration.php";
    //private static final String URL_DATA_REQUEST = "http://104.131.165.12/sisgeer/requisitar_dados.php";
    //private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static final String TAG = ServerRequests.class.getSimpleName();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public ServerRequests(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processando...");
        progressDialog.setMessage("Por favor aguarde!");
    }

    public void registerBackground(Usuario usuario, GetUserCallback callBack) {
        progressDialog.show();
        new registerAsyncTask(usuario, callBack).execute();
    }

    public void setConfigurationBackground(String costEnergy, String voltagePower) {
        progressDialog.show();
        new setConfigurationAsyncTask(costEnergy, voltagePower).execute();
    }


    public void updateBackground(Data data, GetDataCallback callback) {
        progressDialog.show();
        new updateAsyncTask(data, callback).execute();
    }

    public void loginBackground(Usuario usuario, GetUserCallback callBack) {
        progressDialog.show();
        new loginAsyncTask(usuario, callBack).execute();
        Log.d(TAG, "loginBackground()");
    }

    public class setConfigurationAsyncTask extends AsyncTask<Void, Void, Void> {

        String costEnergy, voltagePower;

        public setConfigurationAsyncTask(String costEnergy, String voltagePower) {
            this.costEnergy   = costEnergy;
            this.voltagePower = voltagePower;
        }

        @Override
        protected Void doInBackground(Void... params) {

            RequestBody formBody = new FormEncodingBuilder()
                    .add("costEnergy", costEnergy)
                    .add("voltagePower", voltagePower)
                    .build();

            Request request = new Request.Builder()
                    .url(URL_CONFIGURATION)
                    .post(formBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }

    public class registerAsyncTask extends AsyncTask<Void, Void, Void> {

        Usuario usuario;
        GetUserCallback callBack;

        public registerAsyncTask(Usuario usuario, GetUserCallback callBack) {
            this.usuario = usuario;
            this.callBack = callBack;
        }

        @Override
        protected Void doInBackground(Void... params) {

            RequestBody formBody = new FormEncodingBuilder()
                    .add("nome", usuario.nome)
                    .add("email", usuario.email)
                    .add("login", usuario.login)
                    .add("senha", usuario.senha)
                    .build();

            Request request = new Request.Builder()
                    .url(URL_REGISTER)
                    .post(formBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                Log.d(TAG, request.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            callBack.done(null);
            super.onPostExecute(aVoid);
        }
    }

    public class loginAsyncTask extends AsyncTask<Void, Void, Usuario> {

        Usuario usuario;
        GetUserCallback callBack;

        public loginAsyncTask(Usuario usuario, GetUserCallback callBack) {
            this.usuario = usuario;
            this.callBack = callBack;
        }

        @Override
        protected Usuario doInBackground(Void... params) {

            RequestBody formBody = new FormEncodingBuilder()
                    .add("login", usuario.login)
                    .add("senha", usuario.senha)
                    .build();

            Request request = new Request.Builder()
                    .url(URL_LOGIN)
                    .post(formBody)
                    .build();

            Usuario returnUsuario = null;

            try {
                Response response = client.newCall(request).execute();
                String jsonDados = response.body().string();

                Log.d(TAG, jsonDados);

                JSONObject jsonObject = new JSONObject(jsonDados);

                if (jsonDados.length() == 0) {
                    returnUsuario = null;
                } else {
                    String nome = jsonObject.getString("nome");
                    String email = jsonObject.getString("email");

                    returnUsuario = new Usuario(nome, email, usuario.login, usuario.senha);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "FIM: loginAsyncTask");
            return returnUsuario;
        }

        @Override
        protected void onPostExecute(Usuario returnUsuario) {
            progressDialog.dismiss();
            callBack.done(returnUsuario);
            super.onPostExecute(returnUsuario);
        }
    }

    public class updateAsyncTask extends AsyncTask<Void, Void, Data> {

        Data data;
        GetDataCallback callBack;

        public updateAsyncTask(Data data, GetDataCallback callBack) {
            this.data = data;
            this.callBack = callBack;
        }

        @Override
        protected Data doInBackground(Void... params) {

            Data dataReturned = null;



//            RequestBody requestBody = new FormEncodingBuilder()
//                .add("data", data.date)
//                .add("tensao", data.voltagePower)
//                .add("kwhDiario[0]", data.kwh[0])
//                .add("kwhDiario[1]", data.kwh[1])
//                .add("kwhDiario[2]", data.kwh[2])
//                .add("kwhDiario[3]", data.kwh[3])
//                .add("kwhDiario[4]", data.kwh[4])
//                .add("kwhDiario[5]", data.kwh[5])
//                .add("kwhDiario[6]", data.kwh[6])
//                .add("kwhTotal", data.kwhTotal)
//                .build();

            RequestBody requestBody = RequestBody.create(JSON, "Content-Type: application/json; charset=UTF-8");

            Request request = new Request.Builder()
                .url(URL_UPDATE)
                .post(requestBody)
                .build();

            try {
                Response response = client.newCall(request).execute();
                String jsonData = response.body().string();
                JSONObject jsonObject = new JSONObject(jsonData);

                Log.d("Json Object: ", jsonObject.toString());

                if (jsonData.length() == 0) {
                    dataReturned = null;
                } else {
                    String kwh[] = new String[7];

                    for (int i = 0; i < kwh.length; i++) {
                        kwh[i] = "0.000001";
                    }

                    String date = jsonObject.getString("data");
                    String voltagePower = jsonObject.getString("tensao");
                    String costEnergy = jsonObject.getString("custoEnergia");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("kwhDiario");

                    Log.d("Length:",  String.valueOf(jsonObject1.length()));

                    for (int i = 0; i < jsonObject1.length(); i++) {
                        kwh[i] = jsonObject1.getString(String.valueOf(i+1));
                        Log.d("for:", jsonObject1.getString(String.valueOf(i+1)));
                    }

                    Log.d("DEBUG:", jsonObject1.toString());
                    Log.d("DEBUG:", kwh[0]);

                    String kwhTotal = jsonObject.getString("kwhTotal");

                    dataReturned = new Data();
                    dataReturned.date = date;
                    dataReturned.voltagePower = voltagePower;
                    dataReturned.costEnergy = costEnergy;
                    dataReturned.kwh = kwh;
                    dataReturned.kwhTotal = kwhTotal;

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            return dataReturned;
        }

        @Override
        protected void onPostExecute(Data returnData) {
            progressDialog.dismiss();
            callBack.done(returnData);
            super.onPostExecute(returnData);
        }
    }
}

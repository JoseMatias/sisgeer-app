package com.josematias.sisgeer;

public interface GetDataCallback {
    public abstract void done(Data dataReturned);
}

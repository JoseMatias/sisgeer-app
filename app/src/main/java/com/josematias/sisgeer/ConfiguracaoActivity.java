package com.josematias.sisgeer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ConfiguracaoActivity extends AppCompatActivity implements View.OnClickListener {

    Button bSalvar;
    EditText etCustoEnergia;
    Spinner spnTensao;
    Toast toast;

    int tensao = 0;
    double custoEnergia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        etCustoEnergia = (EditText) findViewById(R.id.etCustoEnergia);
        spnTensao = (Spinner) findViewById(R.id.spnTensao);

        bSalvar = (Button) findViewById(R.id.bSalvar);
        bSalvar.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();

        String voltagePower = bundle.getString("voltagePower");
        String costEnergy = bundle.getString("costEnergy");

        etCustoEnergia.setText(costEnergy);
        spnTensao.setSelection(getIndexSpinner(spnTensao, voltagePower));

        spnTensao.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    tensao = 110;
                else if (position == 1)
                    tensao = 220;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int getIndexSpinner(Spinner spinner, String myString) {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSalvar:
                String costEnergy = etCustoEnergia.getText().toString();
                String voltagePower = spnTensao.getSelectedItem().toString();

                if (!costEnergy.isEmpty() && !voltagePower.isEmpty()) {
                    setConfiguration(costEnergy, voltagePower);

                    toast = Toast.makeText(this, "Configuração salva com sucesso!", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    toast = Toast.makeText(this, "Os campos devem está preenchidos!", Toast.LENGTH_LONG);
                    toast.show();
                }

                break;
        }
    }

    private void setConfiguration(String costEnergy, String voltagePower) {
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.setConfigurationBackground(costEnergy, voltagePower);
    }
}

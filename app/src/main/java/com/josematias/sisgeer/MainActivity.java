package com.josematias.sisgeer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bSair, bGrafico;
    EditText etNome, etEmail, etLogin, etSenha;
    UsuarioBaseDadosLocal usuarioBaseDadosLocal;

    private Toast toast;
    private long lastBackPressTime = 0;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate() Restoring previous state");
        } else {
            Log.d(TAG, "onCreate() No saved state available");
        }


        etNome = (EditText) findViewById(R.id.etNome);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etSenha = (EditText) findViewById(R.id.etSenha);

        bSair = (Button) findViewById(R.id.bSair);
        bGrafico = (Button) findViewById(R.id.bGrafico);

        bSair.setOnClickListener(this);
        bGrafico.setOnClickListener(this);

        usuarioBaseDadosLocal = new UsuarioBaseDadosLocal(this);

        Log.d(TAG, "Teste ttt");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.d(TAG, "Entrou: "+item.toString());

        switch (item.getItemId()) {
            case R.id.action_configuracao:
                toast = Toast.makeText(this,"Configurações selecionadas", Toast.LENGTH_LONG);
                toast.show();
                startActivity(new Intent(this, ConfiguracaoActivity.class));
                break;

            case R.id.action_sobre:
                toast = Toast.makeText(this,"Sobre selecionado.", Toast.LENGTH_LONG);
                toast.show();
                break;

            case R.id.action_atualizacao:
                toast = Toast.makeText(this,"Atualizando...", Toast.LENGTH_LONG);
                toast.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (autenticacao()) {
            exibirDetalhesUsuario();
        } else {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }
    }

    private boolean autenticacao() {
        return usuarioBaseDadosLocal.getUsuarioLogado();
    }

    private void exibirDetalhesUsuario () {
        Usuario usuario = usuarioBaseDadosLocal.getDadosUsuario();

        etNome.setText(usuario.nome);
        etEmail.setText(usuario.email);
        etLogin.setText(usuario.login);
        etSenha.setText(usuario.senha);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSair:
                usuarioBaseDadosLocal.limparDadosUsuario();
                usuarioBaseDadosLocal.setUsuarioLogado(false);
                startActivity(new Intent(this, LoginActivity.class));
                break;

            case R.id.bGrafico:
                startActivity(new Intent(this, ChartActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
                usuarioBaseDadosLocal.limparDadosUsuario();
                usuarioBaseDadosLocal.setUsuarioLogado(false);

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            super.onBackPressed();
        }
    }
}

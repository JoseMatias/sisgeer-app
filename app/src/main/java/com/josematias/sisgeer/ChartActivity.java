package com.josematias.sisgeer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Vector;


public class ChartActivity extends AppCompatActivity {

    private Data data;
    private BarChart barChart;
    private BarData barData;
    private TextView tvAtualizacao, tvConsumoKwh, tvValorReal;
    private Toast toast;
    private long lastBackPressTime = 0;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts);

        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate(): Restaurar estado anterior.");
        } else {
            Log.d(TAG, "onCreate(): Nenhum estado salvo disponível.");
        }

        tvAtualizacao = (TextView) findViewById(R.id.tvAtualizacao);
        tvConsumoKwh = (TextView) findViewById(R.id.tvConsumoKwh);
        tvValorReal = (TextView) findViewById(R.id.tvValorReal);

        barChart      = (BarChart) findViewById(R.id.chart);

        data = new Data();

        updateData();

        barChart.setBackgroundColor(Color.TRANSPARENT);
        barChart.getLegend().setEnabled(false);
        barChart.setDescription("");

        /* Desabilitar toda interação com o gráfico. */
        barChart.setPinchZoom(false);
        barChart.setDrawHighlightArrow(false);
        barChart.setHighlightPerTapEnabled(false);
        barChart.setHighlightPerDragEnabled(false);
        barChart.setDoubleTapToZoomEnabled(false);

        /* Configuration graphics xAxis. */
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setTextColor(Color.WHITE);
        //leftAxis.setSpaceTop(15f);
        //leftAxis.setAxisMinValue(0f);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawLabels(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_configuracao:
                toast = Toast.makeText(this,"Configurações selecionadas", Toast.LENGTH_LONG);
                toast.show();

                Intent intent = new Intent(this, ConfiguracaoActivity.class);
                intent.putExtra("voltagePower", data.voltagePower);
                intent.putExtra("costEnergy", data.costEnergy);

                startActivity(intent);
                break;

            case R.id.action_sobre:
                toast = Toast.makeText(this,"Sobre selecionado.", Toast.LENGTH_LONG);
                toast.show();
                break;

            case R.id.action_atualizacao:
                //Dados dados = new Dados();
                updateData();

                toast = Toast.makeText(this,"Atualizando...", Toast.LENGTH_LONG);
                toast.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateData() {
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.updateBackground(data, new GetDataCallback() {
            @Override
            public void done(Data dataReturnend) {
                if (dataReturnend == null) {
                    showErrorMessage("Falha durante atualização!");
                } else {
                    data.setData(dataReturnend);

                    tvAtualizacao.setText("Última atualização: " + data.date);
                    tvConsumoKwh.setText(data.kwhTotal);
                    double valor = Double.parseDouble(data.kwhTotal) * Double.parseDouble(data.costEnergy);
                    tvValorReal.setText(String.valueOf(valor));
                    barData = new BarData(labels(data.date), dataSet(dataReturnend.kwh));
                    barChart.setData(barData);
                    //barChart.animateY(2000);
                }
            }
        });
    }

    private ArrayList<String> labels(String date) {
        ArrayList<String> xAxisLabels = new ArrayList<>();
        int index = 0;

        ArrayList<String> weekDays = new ArrayList();
            weekDays.add("Seg");
            weekDays.add("Ter");
            weekDays.add("Qua");
            weekDays.add("Qui");
            weekDays.add("Sex");
            weekDays.add("Sab");
            weekDays.add("Dom");

        switch (date.substring(0, 3)) {
            case "Seg": index = 1; break;
            case "Ter": index = 2; break;
            case "Qua": index = 3; break;
            case "Qui": index = 4; break;
            case "Sex": index = 5; break;
            case "Sab": index = 6; break;
            case "Dom": index = 7; break;
        }

        for (int i = 0; i < 7; i++, index++) {
            xAxisLabels.add(weekDays.get(index));
            if (index == 6) index = -1;
        }

        return xAxisLabels;
    }

    private BarDataSet dataSet(String[] kwh) {
        ArrayList<BarEntry> entries = new ArrayList<>();
        int color = Color.rgb(204,229,255);

        entries.add(new BarEntry(Float.parseFloat(kwh[6]), 0));
        entries.add(new BarEntry(Float.parseFloat(kwh[5]), 1));
        entries.add(new BarEntry(Float.parseFloat(kwh[4]), 2));
        entries.add(new BarEntry(Float.parseFloat(kwh[3]), 3));
        entries.add(new BarEntry(Float.parseFloat(kwh[2]), 4));
        entries.add(new BarEntry(Float.parseFloat(kwh[1]), 5));
        entries.add(new BarEntry(Float.parseFloat(kwh[0]), 6));

        BarDataSet dataset = new BarDataSet(entries,"");

        dataset.setColors(new int[]{color, color, color, color, color, color, Color.rgb(255,153,153)});
        dataset.setValueTextColor(Color.WHITE);

        return dataset;
    }

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
                //usuarioBaseDadosLocal.limparDadosUsuario();
                //usuarioBaseDadosLocal.setUsuarioLogado(false);

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                Log.d(TAG, "onBackPressed(): Finaliza Aplicativo.");
            }

            super.onBackPressed();
        }
    }

    private void showErrorMessage(String msg) {
        AlertDialog.Builder dialogBuider = new AlertDialog.Builder(ChartActivity.this);

        dialogBuider.setMessage(msg);
        dialogBuider.setPositiveButton("Ok", null);
        dialogBuider.show();
    }
}

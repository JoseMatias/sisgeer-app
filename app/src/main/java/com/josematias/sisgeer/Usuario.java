package com.josematias.sisgeer;

public class Usuario {
    String nome, email, login, senha;

    public Usuario(String nome, String email, String login, String senha) {
        this.nome = nome;
        this.email = email;
        this.login = login;
        this.senha = senha;
    }

    public Usuario(String login, String senha) {
        this.nome = "";
        this.email = "";
        this.login = login;
        this.senha = senha;
    }
}

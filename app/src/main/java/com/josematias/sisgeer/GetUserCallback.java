package com.josematias.sisgeer;

interface GetUserCallback {
    public abstract void done(Usuario usuarioRetornado);
}

package com.josematias.sisgeer;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterUserActivity extends AppCompatActivity implements View.OnClickListener {

    Button bSalvar;
    EditText etNome, etEmail, etLogin, etSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        etNome = (EditText) findViewById(R.id.etNome);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etSenha = (EditText) findViewById(R.id.etSenha);
        bSalvar = (Button) findViewById(R.id.bSalvar);

        bSalvar.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSalvar:
                String nome = etNome.getText().toString();
                String email = etEmail.getText().toString();
                String login = etLogin.getText().toString();
                String senha = etSenha.getText().toString();

                Usuario usuario = new Usuario(nome, email, login, senha);

                registrarUsuario(usuario);
            break;

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
            break;
        }
    }

    private void registrarUsuario(Usuario usuario) {
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.registerBackground(usuario, new GetUserCallback() {
            @Override
            public void done(Usuario usuarioRetornado) {
                startActivity(new Intent(RegisterUserActivity.this, LoginActivity.class));
            }
        });
    }
}

package com.josematias.sisgeer;

import android.content.Context;
import android.content.SharedPreferences;

public class DadosLocal {

    public static final String SP_NAME = "DadosRecebidos";
    SharedPreferences dadosLocal;

    public DadosLocal (Context context) {
        dadosLocal = context.getSharedPreferences(SP_NAME, 0);
    }

    public void SetDados(Data data) {
        SharedPreferences.Editor spEditor = dadosLocal.edit();

        spEditor.putString("data", data.date);
//        spEditor.putString("email", usuario.email);
//        spEditor.putString("login", usuario.login);
//        spEditor.putString("senha", usuario.senha);
        spEditor.commit();
    }

    public Data getDados() {
        String date = dadosLocal.getString("data", "");
//        String email = usuarioBaseDadosLocal.getString("email", "");
//        String login = usuarioBaseDadosLocal.getString("login", "");
//        String senha = usuarioBaseDadosLocal.getString("senha", "");

        Data data = new Data();
        data.date = date;

        return data;
    }

    public void limparDados() {
        SharedPreferences.Editor spEditor = dadosLocal.edit();

        spEditor.clear();
        spEditor.commit();
    }
}

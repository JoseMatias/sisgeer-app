package com.josematias.sisgeer;

public interface SetConfigurationCallback {
    public abstract void done(String costEnergy, String voltagePower);
}

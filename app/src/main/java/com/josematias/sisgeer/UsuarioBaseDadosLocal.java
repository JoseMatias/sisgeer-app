package com.josematias.sisgeer;

import android.content.Context;
import android.content.SharedPreferences;

public class UsuarioBaseDadosLocal {

    public static final String SP_NAME = "InfoUsuario";
    SharedPreferences usuarioBaseDadosLocal;

    public UsuarioBaseDadosLocal (Context context) {
        usuarioBaseDadosLocal = context.getSharedPreferences(SP_NAME, 0);
    }

    public void SetDadosUsuario(Usuario usuario) {
        SharedPreferences.Editor spEditor = usuarioBaseDadosLocal.edit();

        spEditor.putString("nome", usuario.nome);
        spEditor.putString("email", usuario.email);
        spEditor.putString("login", usuario.login);
        spEditor.putString("senha", usuario.senha);
        spEditor.commit();
    }

    public Usuario getDadosUsuario() {
        String nome = usuarioBaseDadosLocal.getString("nome", "");
        String email = usuarioBaseDadosLocal.getString("email", "");
        String login = usuarioBaseDadosLocal.getString("login", "");
        String senha = usuarioBaseDadosLocal.getString("senha", "");

        Usuario usuarioLogado = new Usuario(nome, email, login, senha);

        return usuarioLogado;
    }

    public void setUsuarioLogado(boolean logado) {
        SharedPreferences.Editor spEditor = usuarioBaseDadosLocal.edit();

        spEditor.putBoolean("logado", logado);
        spEditor.commit();
    }

    public boolean getUsuarioLogado() {
        if (usuarioBaseDadosLocal.getBoolean("logado", false) == true) {
            return true;
        } else {
            return false;
        }
    }

    public void limparDadosUsuario() {
        SharedPreferences.Editor spEditor = usuarioBaseDadosLocal.edit();

        spEditor.clear();
        spEditor.commit();
    }
}
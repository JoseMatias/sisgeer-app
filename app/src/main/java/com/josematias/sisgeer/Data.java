package com.josematias.sisgeer;

public class Data {
    public String kwh[] = new String[7];
    public String kwhTotal = "";
    public String date = "";
    public String costEnergy = "";
    public String voltagePower = "";

    public void setData(Data data) {
        this.date = data.date;
        this.voltagePower = data.voltagePower;
        this.costEnergy = data.costEnergy;
        this.kwh = data.kwh;
        this.kwhTotal = data.kwhTotal;
    }
}

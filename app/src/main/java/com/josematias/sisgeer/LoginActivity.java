package com.josematias.sisgeer;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.nfc.Tag;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button bEntrar;
    EditText etLogin, etSenha;
    TextView tvCdtUsuario;
    UsuarioBaseDadosLocal usuarioBaseDadosLocal;

    private Toast toast;
    private long lastBackPressTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLogin = (EditText) findViewById(R.id.etLogin);
        etSenha = (EditText) findViewById(R.id.etSenha);
        bEntrar = (Button) findViewById(R.id.bEntrar);
        tvCdtUsuario = (TextView) findViewById(R.id.tvCdtUsuario);

        bEntrar.setOnClickListener(this);
        tvCdtUsuario.setOnClickListener(this);

        usuarioBaseDadosLocal = new UsuarioBaseDadosLocal(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bEntrar:
                if (verificaConexao()) {
                    String login = etLogin.getText().toString();
                    String senha = etSenha.getText().toString();

                    Usuario usuario = new Usuario(login, senha);

                    authenticate(usuario);
                    //startActivity(new Intent(this, ChartActivity.class));
                } else {
                    showErrorMessage("Sem conexão com a internet!");
                }
                break;

            case R.id.tvCdtUsuario:
                startActivity(new Intent(this, RegisterUserActivity.class));
                break;
        }
    }

    private void authenticate(Usuario usuario) {
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.loginBackground(usuario, new GetUserCallback() {
            @Override
            public void done(Usuario usuarioRetornado) {
                if (usuarioRetornado == null) {
                    showErrorMessage("Login ou senha incorreto!");
                } else {
                    Log.d("authenticate() :", "chegou aqui 65465465");
                    loginUsuario(usuarioRetornado);
                    Log.d("authenticate() :", "chegou aqui!");
                }
            }
        });
    }

    private void showErrorMessage(String msg) {
        AlertDialog.Builder dialogBuider = new AlertDialog.Builder(LoginActivity.this);

        dialogBuider.setMessage(msg);
        dialogBuider.setPositiveButton("Ok", null);
        dialogBuider.show();
    }

    private void loginUsuario(Usuario usuarioRetornado) {
        usuarioBaseDadosLocal.SetDadosUsuario(usuarioRetornado);
        usuarioBaseDadosLocal.setUsuarioLogado(true);

        Log.d("teste", usuarioRetornado.nome);

        startActivity(new Intent(this, ChartActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            super.onBackPressed();
        }
    }

    /* Função para verificar existência de conexão com a internet. */
    public  boolean verificaConexao() {
        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
